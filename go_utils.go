package utils

import (
	"os"
	"strconv"
	"strings"
	"syscall"
	"time"
)

var loc *time.Location

func Restart() {
	executable, getErr := os.Executable()
	if getErr != nil {
		panic(getErr)
	}

	args := os.Args[:]
	startErr := syscall.Exec(executable, args, os.Environ())
	if startErr != nil {
		panic(startErr)
	}
}

func ToListOfIntegers(str string) ([]int, error) {
	converted := make([]int, 0)
	splitStr := strings.Split(str, ",")
	for _, pivot := range splitStr {
		result, err := strconv.Atoi(pivot)
		if err != nil {
			return nil, err
		}
		converted = append(converted, result)
	}
	return converted, nil
}

func CleanString(str string) string {
	// Replace all newlines and carriage returns with a space
	str = strings.ReplaceAll(str, "\n", " ")
	str = strings.ReplaceAll(str, "\r", " ")

	// Replace all double spaces with a single space
	str = strings.ReplaceAll(str, "  ", " ")

	// Trim the response string
	return strings.TrimSpace(str)
}

func Float64ToGermanString(f float64) string {
	str := strconv.FormatFloat(f, 'f', 2, 64)
	str = strings.ReplaceAll(str, ".", ",")
	return str
}

func Contains(slice []string, str string) bool {
	for _, s := range slice {
		if s == str {
			return true
		}
	}
	return false
}

// Compare -1 if t1 is before t2; 0 if equal; 1 if t1 is after t2;
// 1 if t1 is nil and t2 not nil; 0 if both are nil; -1 if t1 is not nil and t2 is nil
func Compare(t1 *time.Time, t2 *time.Time) int {
	if t1 != nil {
		if t2 != nil {
			if t1.Equal(*t2) {
				return 0
			} else if t1.Before(*t2) {
				return -1
			} else {
				return 1
			}
		} else {
			return -1
		}
	} else if t2 != nil {
		return 1
	} else {
		return 0
	}
}

func RoundToStartOfDay(t time.Time) time.Time {
	// Set to full day
	t2 := t.In(loc)
	t2 = time.Date(t2.Year(), t2.Month(), t2.Day(), 0, 0, 0, 0, loc)

	return t2.In(loc)
}

// GetNextHalfMonthDay returns the 15th of the month, if before 15th. Returns the end of month, if after 15th.
func GetNextHalfMonthDay(t time.Time) time.Time {
	localTime := t.In(time.Local)
	currentYear, currentMonth, currentDay := localTime.Date()
	if currentDay < 15 {
		return time.Date(currentYear, currentMonth, 15, 23, 59, 0, 0, time.Local)
	} else {
		return time.Date(currentYear, currentMonth+1, 0, 23, 59, 0, 0, time.Local)
	}
}

func CloserToEndOfDay(t time.Time) bool {
	hour := t.Hour()
	minute := t.Minute()
	second := t.Second()

	secondsSinceStart := hour*3600 + minute*60 + second
	secondsUntilEnd := 86400 - secondsSinceStart

	return secondsSinceStart <= secondsUntilEnd
}

func init() {
	var err error
	loc, err = time.LoadLocation("Europe/Berlin")
	if err != nil {
		panic(err)
	}
}
