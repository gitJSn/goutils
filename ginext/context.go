package ginext

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	errors "gitlab.com/PointDuck/goutils/errorsext"
	"gitlab.com/PointDuck/goutils/filter"
)

type Context struct {
	*gin.Context
}

type CustomHandlerFunc func(*Context)

func CustomContextWrapper(handler CustomHandlerFunc) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := &Context{c}
		handler(ctx)
	}
}

func (ctx *Context) BindQueryParamsToFilter(out filter.ForRepository) errors.WithCode {
	if err := ctx.ShouldBindQuery(out); err != nil {
		return errors.Create(errors.INVALID_QUERY_PARAM_FORMAT, err)
	}
	out.SetDefaults()
	return nil
}

func (ctx *Context) BindToStruct(out any) errors.WithCode {
	if err := ctx.ShouldBind(out); err != nil {
		return errors.CreateWithValidationErrors(errors.INVALID_BODY_FORMAT, err, out)
	}
	return nil
}

func (ctx *Context) BindQueryParamToInt(path string, out *int, def string) errors.WithCode {
	s := ctx.DefaultQuery(path, def)
	i, err := strconv.Atoi(s)
	if err != nil {
		return errors.Create(errors.INVALID_QUERY_PARAM_FORMAT, fmt.Errorf("query parameter '%s' is invalid: %v", path, err))
	}
	*out = i
	return nil
}

func (ctx *Context) BindQueryParamToString(path string, out *string) {
	*out = ctx.Query(path)
}

func (ctx *Context) MustBindQueryParamToInt(path string, out *int) errors.WithCode {
	s := ctx.Query(path)
	i, err := strconv.Atoi(s)
	if err != nil {
		return errors.Create(errors.INVALID_QUERY_PARAM_FORMAT, fmt.Errorf("query parameter '%s' is invalid: %v", path, err))
	}
	*out = i
	return nil
}

func (ctx *Context) BindQueryParamToIntArray(path string, out *[]int, def string) errors.WithCode {
	s := ctx.DefaultQuery(path, def)
	is := strings.Split(s, ",")
	ii := make([]int, len(is))
	var err error
	for index, pivot := range is {
		ii[index], err = strconv.Atoi(pivot)
		if err != nil {
			return errors.Create(errors.INVALID_QUERY_PARAM_FORMAT, fmt.Errorf("query parameter '%s' is invalid: %v", path, err))
		}
	}
	*out = ii
	return nil
}

func (ctx *Context) BindQueryParamToBool(path string, out *bool, def string) errors.WithCode {
	s := ctx.DefaultQuery(path, def)
	b, err := strconv.ParseBool(s)
	if err != nil {
		return errors.Create(errors.INVALID_QUERY_PARAM_FORMAT, fmt.Errorf("query parameter '%s' is invalid: %v", path, err))
	}
	*out = b
	return nil
}

func (ctx *Context) BindQueryParamToTime(path string, out *time.Time) errors.WithCode {
	var s string
	if s = ctx.Query(path); s == "" {
		return errors.Create(errors.INVALID_QUERY_PARAM_FORMAT, fmt.Errorf("query parameter '%s' is missing", path))
	}
	t, err := time.Parse(time.RFC3339, s)
	if err != nil {
		return errors.Create(errors.INVALID_QUERY_PARAM_FORMAT, fmt.Errorf("query parameter '%s' is invalid: %v", path, err))
	}
	*out = t
	return nil
}

func (ctx *Context) OptionalBindQueryParamToTime(path string, out **time.Time) errors.WithCode {
	var s string
	if s = ctx.Query(path); s == "" {
		return nil
	}
	t, err := time.Parse(time.RFC3339, s)
	if err != nil {
		return errors.Create(errors.INVALID_QUERY_PARAM_FORMAT, fmt.Errorf("query parameter '%s' is invalid: %v", path, err))
	}
	*out = &t
	return nil
}

func (ctx *Context) BindURIToUint(path string, id *uint) errors.WithCode {
	i, err := strconv.Atoi(ctx.Param(path))
	if err != nil {
		return errors.Create(errors.INVALID_ID, fmt.Errorf("path parameter '%s' is invalid: %v", path, err))
	}
	intID := i
	*id = uint(intID)
	return nil
}

func (ctx *Context) BindURIToInt(path string, id *int) errors.WithCode {
	i, err := strconv.Atoi(ctx.Param(path))
	if err != nil {
		return errors.Create(errors.INVALID_ID, fmt.Errorf("path parameter '%s' is invalid: %v", path, err))
	}
	*id = i
	return nil
}

func (ctx *Context) BindURIToTime(path string, month *time.Time) errors.WithCode {
	s := ctx.Param(path)
	// in format 2023-10-31T23:00:00Z
	t, err := time.Parse(time.RFC3339, s)
	if err != nil {
		return errors.Create(errors.INVALID_ID, fmt.Errorf("path parameter '%s' is invalid: %v", path, err))
	}
	*month = t
	return nil
}

type errorResponse struct {
	Error            string                  `json:"error"`
	GolangError      string                  `json:"golangError"`
	ValidationErrors errors.ValidationErrors `json:"validationErrors"`
}

func (ctx *Context) RespondErrorFromCode(withCode errors.WithCode) {
	code := withCode.Code()
	err := withCode.Error()
	status, message := code.CodeAndMessage()

	responseMessage := errorResponse{
		Error:            message,
		GolangError:      err.Error(),
		ValidationErrors: withCode.ValidationErrors(),
	}
	ctx.AbortWithStatusJSON(status, responseMessage)
}

// RespondError sends an error message with the status code to context
func (ctx *Context) RespondError(code errors.Code, err error) {
	ctx.RespondErrorFromCode(errors.Create(code, err))
}

// Unauthorized sends a message with the status code to context
func (ctx *Context) Unauthorized(status int, message string) {
	responseMessage := map[string]string{
		"message": message,
	}
	ctx.AbortWithStatusJSON(status, responseMessage)
}

// RespondSuccess sends a JSON version of the response interface with code 200 OK
func (ctx *Context) RespondSuccess(response interface{}) {
	ctx.JSON(http.StatusOK, response)
}

// RespondCreated sends a JSON version of the response interface with code 201 CREATED
func (ctx *Context) RespondCreated(response interface{}) {
	ctx.JSON(http.StatusCreated, response)
}
