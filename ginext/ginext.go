package ginext

import (
	errors "gitlab.com/PointDuck/goutils/errorsext"

	"github.com/jinzhu/copier"
)

// BindOnto uses a generic type T to which the Context BindOnto then copies it onto the object
func BindOnto[T any](ctx *Context, toValue any) errors.WithCode {
	var obj T
	if err := ctx.ShouldBindJSON(&obj); err != nil {
		return errors.CreateWithValidationErrors(errors.INVALID_BODY_FORMAT, err, obj)
	}
	if err := copier.Copy(toValue, &obj); err != nil {
		return errors.Create(errors.FAILED_CONVERTING_BODY, err)
	}
	return nil
}
