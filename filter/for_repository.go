package filter

import "gorm.io/gorm"

type ForRepository interface {
	SetDefaults()
	Aggregate() bool
	Apply(db *gorm.DB) *gorm.DB
}
