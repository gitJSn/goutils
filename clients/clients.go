package clients

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"

	utils "gitlab.com/PointDuck/goutils"
)

type HTTPClient interface {
	Do(req *http.Request) (*http.Response, error)
	Post(url string, data any) ([]byte, error)
	PostNoBody(url string) error
}

type StandardHTTPClient struct {
	*http.Client
}

func NewClient(client *http.Client) HTTPClient {
	return &StandardHTTPClient{
		client,
	}
}

func (client *StandardHTTPClient) Post(url0 string, data any) ([]byte, error) {
	// Create URL
	url1, err := url.Parse(url0)
	if err != nil {
		return nil, fmt.Errorf("failed creating url: %w", err)
	}

	// Marshal data into JSON
	jsonData, err := json.Marshal(data)
	if err != nil {
		return nil, fmt.Errorf("failed marshalling data: %w", err)
	}

	// Create http request
	req, err := http.NewRequest("POST", url1.String(), bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, fmt.Errorf("failed creating request: %w", err)
	}

	// Set header to inform server we're sending JSON data
	req.Header.Set("Content-Type", "application/json")

	// Send the request using http client
	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed sending request: %w", err)
	}
	defer resp.Body.Close()

	// Read the response body
	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed reading response: %w", err)
	}

	// Check the status code
	if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
		return respBody, nil
	} else {
		cleanedResponseBody := utils.CleanString(string(respBody))
		return nil, fmt.Errorf("response was not 200: %s", cleanedResponseBody)
	}
}

func (client *StandardHTTPClient) PostNoBody(url0 string) error {
	// Create URL
	url1, err := url.Parse(url0)
	if err != nil {
		return fmt.Errorf("failed creating url: %w", err)
	}

	// Create http request
	req, err := http.NewRequest("POST", url1.String(), nil)
	if err != nil {
		return fmt.Errorf("failed creating request: %w", err)
	}

	// Set header to inform server we're sending JSON data
	req.Header.Set("Content-Type", "application/json")

	// Send the request using http client
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("failed sending request: %w", err)
	}
	defer resp.Body.Close()

	// Read the response body
	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("failed reading response: %w", err)
	}

	// Check the status code
	if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
		return nil
	} else {
		cleanedResponseBody := utils.CleanString(string(respBody))
		return fmt.Errorf("response was not 200: %s", cleanedResponseBody)
	}
}
