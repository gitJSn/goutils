package errorsext

import (
	"net/http"
)

type Code int32

const (
	UNAUTHORIZED Code = iota
	INVALID_ID
	INVALID_PASSWORD
	INVALID_BODY_FORMAT
	INVALID_URI_FORMAT
	INVALID_QUERY_PARAM_FORMAT
	USER_ALREADY_EXISTS
	FAILED_CREATING_SQL
	FAILED_EXECUTING_QUERY
	FAILED_CONVERTING_QUERY_RESULT
	CLOSED_DB_CONNECTION
	NOT_FOUND
	CREATE_ONE_FAILED
	UPDATE_ONE_FAILED
	UPDATE_MANY_FAILED
	DELETE_ONE_FAILED
	CREATE_MANY_FAILED
	FAILED_CONVERTING_BODY
	METHOD_NOT_IMPLEMENTED
	UPSERT_FAILED
	CREATING_AUTH_FAILED
	FAILED_CREATING_URL
	FAILED_CREATING_REQUEST
	FAILED_SENDING_REQUEST
	FAILED_READING_RESPONSE
	RESPONSE_NOT_200
	MISSING_ENV_VALUE
	FAILED_MARSHALING_JSON
	WEBSOCKET_CLOSING_ERROR
	WEBSOCKET_WRITE_FAILED
	MISSING_PASSWORD
	FILTER_DEFAULTS_NOT_SET
	INVALID_FILTER_FORMAT
	SCHEDULER_ERROR
	MISSING_ID
	WRITE_ERROR
	INVALID_CLAIMS
	DATABASE_ERROR
	INVALID_FIELDS
	INVALID_DATES
	FAILED_ADDING_JOB
	FAILED_REMOVING_JOB
	JOB_EXECUTION_FAILED
	JOB_STOPPING_FAILED
	PDF_GENERATION_FAILED
)

type WithCode interface {
	Code() Code
	Error() error
	ValidationErrors() ValidationErrors
}

type WithCodeImplementation struct {
	C  Code
	E  error
	VE ValidationErrors
}

func (impl *WithCodeImplementation) Code() Code {
	return impl.C
}

func (impl *WithCodeImplementation) Error() error {
	return impl.E
}

func (impl *WithCodeImplementation) ValidationErrors() ValidationErrors {
	return impl.VE
}

// CodeAndMessage returns the error code and message
func (e Code) CodeAndMessage() (int, string) {
	switch e {
	default:
		return http.StatusBadRequest, "Missing error code."
	case INVALID_ID:
		return http.StatusBadRequest, "Invalid id."
	case UNAUTHORIZED:
		return http.StatusUnauthorized, "You are not authorized for this."
	case INVALID_PASSWORD:
		return http.StatusBadRequest, "Invalid password."
	case INVALID_BODY_FORMAT:
		return http.StatusBadRequest, "Invalid body format. Could not convert JSON body to model."
	case INVALID_QUERY_PARAM_FORMAT:
		return http.StatusBadRequest, "Invalid query parameter format."
	case INVALID_URI_FORMAT:
		return http.StatusBadRequest, "Invalid uri format."
	case USER_ALREADY_EXISTS:
		return http.StatusConflict, "Username already taken."
	case FAILED_CREATING_SQL:
		return http.StatusInternalServerError, "Failed creating SQL statement."
	case FAILED_EXECUTING_QUERY:
		return http.StatusInternalServerError, "Failed executing query."
	case FAILED_CONVERTING_QUERY_RESULT:
		return http.StatusInternalServerError, "Failed converting query result to model."
	case CLOSED_DB_CONNECTION:
		return http.StatusInternalServerError, "DB connection closed."
	case NOT_FOUND:
		return http.StatusNotFound, "Resource not found."
	case CREATE_ONE_FAILED:
		return http.StatusInternalServerError, "Creating one db entry failed."
	case UPDATE_ONE_FAILED:
		return http.StatusInternalServerError, "Updating one db entry failed."
	case UPDATE_MANY_FAILED:
		return http.StatusInternalServerError, "Updating multiple db entries failed."
	case DELETE_ONE_FAILED:
		return http.StatusInternalServerError, "Deleting one db entry failed."
	case CREATE_MANY_FAILED:
		return http.StatusInternalServerError, "Creating many db entries failed."
	case FAILED_CONVERTING_BODY:
		return http.StatusBadRequest, "Failed converting body."
	case METHOD_NOT_IMPLEMENTED:
		return http.StatusNotImplemented, "Method not implemented."
	case UPSERT_FAILED:
		return http.StatusInternalServerError, "Upsert failed."
	case CREATING_AUTH_FAILED:
		return http.StatusInternalServerError, "Creating auth failed."
	case FAILED_CREATING_URL:
		return http.StatusInternalServerError, "Failed creating url."
	case FAILED_CREATING_REQUEST:
		return http.StatusInternalServerError, "Failed creating request."
	case FAILED_SENDING_REQUEST:
		return http.StatusInternalServerError, "Failed sending request."
	case FAILED_READING_RESPONSE:
		return http.StatusInternalServerError, "Failed reading response."
	case RESPONSE_NOT_200:
		return http.StatusInternalServerError, "Response was not 200."
	case MISSING_ENV_VALUE:
		return http.StatusInternalServerError, "Missing environment value."
	case FAILED_MARSHALING_JSON:
		return http.StatusInternalServerError, "Failed marshaling JSON."
	case WEBSOCKET_CLOSING_ERROR:
		return http.StatusInternalServerError, "Error closing websocket."
	case WEBSOCKET_WRITE_FAILED:
		return http.StatusInternalServerError, "Failed websocket write."
	case MISSING_PASSWORD:
		return http.StatusBadRequest, "Missing password."
	case FILTER_DEFAULTS_NOT_SET:
		return http.StatusInternalServerError, "Filter defaults were not set by server."
	case INVALID_FILTER_FORMAT:
		return http.StatusBadRequest, "Invalid filter format."
	case SCHEDULER_ERROR:
		return http.StatusInternalServerError, "Scheduler error."
	case MISSING_ID:
		return http.StatusBadRequest, "ID missing."
	case WRITE_ERROR:
		return http.StatusInternalServerError, "Write error."
	case INVALID_CLAIMS:
		return http.StatusBadRequest, "Invalid claims."
	case DATABASE_ERROR:
		return http.StatusInternalServerError, "Database error."
	case INVALID_FIELDS:
		return http.StatusConflict, "Invalid fields."
	case INVALID_DATES:
		return http.StatusBadRequest, "Invalid dates."
	case FAILED_ADDING_JOB:
		return http.StatusInternalServerError, "Failed adding job."
	case FAILED_REMOVING_JOB:
		return http.StatusInternalServerError, "Failed removing job."
	case JOB_EXECUTION_FAILED:
		return http.StatusInternalServerError, "Job execution failed."
	case JOB_STOPPING_FAILED:
		return http.StatusInternalServerError, "Job stopping failed."
	case PDF_GENERATION_FAILED:
		return http.StatusInternalServerError, "PDF generation failed."
	}

}
