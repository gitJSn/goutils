package errorsext

import (
	"fmt"
	"reflect"

	"github.com/go-playground/validator/v10"
)

func CreateWithValidationErrors(code Code, err error, obj interface{}) WithCode {
	validationErrors := mapValidationErrors(err, obj)
	return &WithCodeImplementation{
		C:  code,
		E:  err,
		VE: validationErrors,
	}
}

func Create(code Code, err error) WithCode {
	validationErrors := make(map[string]string)
	return &WithCodeImplementation{
		C:  code,
		E:  err,
		VE: validationErrors,
	}
}

type ValidationErrors map[string]string

func mapValidationErrors(err error, obj interface{}) ValidationErrors {
	validationErrorMessages := make(map[string]string)

	if validationErrors, ok := err.(validator.ValidationErrors); ok {
		fieldJsonAnnotations, err := extractFieldsAndTags(obj, "", "")
		if err != nil {
			return validationErrorMessages
		}

		for _, validationError := range validationErrors {
			structNamespace := validationError.StructNamespace()
			paramText := validationError.Param()
			if paramText == "" {
				paramText = ""
			} else {
				paramText = fmt.Sprintf(" %s", paramText)
			}
			fieldName := fieldJsonAnnotations[structNamespace]
			tagText := mapValidationTagToText(validationError.Tag())
			message := fmt.Sprintf("%s%s", tagText, paramText)
			validationErrorMessages[fieldName] = message
		}
	}

	return validationErrorMessages
}

func mapValidationTagToText(tag string) string {
	switch tag {
	case "required":
		return "missing"
	case "max":
		return "max is"
	case "min":
		return "min is"
	case "email":
		return "must be email"
	case "len":
		return "length equal to"
	default:
		return tag
	}
}

// extractFieldsAndTags extracts the fields and json tags from a struct
func extractFieldsAndTags(input interface{}, prefix string, jsonPrefix string) (map[string]string, error) {
	fieldsMap := make(map[string]string)
	val := reflect.ValueOf(input)
	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}
	if val.Kind() != reflect.Struct {
		return nil, fmt.Errorf("input is not a struct or a pointer to struct")
	}
	typ := val.Type()
	if prefix == "" {
		prefix = typ.Name() + "."
	}
	for i := 0; i < val.NumField(); i++ {
		field := typ.Field(i)
		fieldVal := val.Field(i)

		// Construct the namespace (path)
		namespace := prefix + field.Name

		// Construct the json tag
		jsonTag := jsonPrefix + field.Tag.Get("json")
		if jsonTag == jsonPrefix {
			jsonTag = jsonPrefix + field.Name
		}

		// Check if the field is a struct and not a primitive type
		if fieldVal.Kind() == reflect.Struct {
			// Recursively extract fields from the nested struct
			nestedFields, err := extractFieldsAndTags(fieldVal.Interface(), namespace+".", jsonTag+".")
			if err != nil {
				return nil, err
			}
			for k, v := range nestedFields {
				fieldsMap[k] = v
			}
		}
		fieldsMap[namespace] = jsonTag
	}
	return fieldsMap, nil
}
