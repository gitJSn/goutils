package gormext

import (
	"gorm.io/gorm"
)

type Preloadable interface {
	Preload(db *gorm.DB) *gorm.DB
}
