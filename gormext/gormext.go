package gormext

import (
	"gitlab.com/PointDuck/goutils/filter"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type Repository[T Preloadable] interface {
	GetAll(filter filter.ForRepository) ([]T, error)
	GetByID(id uint) (*T, error)
	Create(content *T) error
	CreateInBatches(contents []T, batchSize int) error
	Update(content *T) error
	UpdateInBatches(contents []T, batchSize int) error
	Delete(id uint) error
	DeleteInBatches(ids []uint) error
	Query() *gorm.DB
}

type RepositoryImplementation[T Preloadable] struct {
	instance T
	db       *gorm.DB
}

func New[T Preloadable](db *gorm.DB) Repository[T] {
	return &RepositoryImplementation[T]{db: db}
}

func NewWithOnConflict[T Preloadable](db *gorm.DB, onConflict clause.OnConflict) Repository[T] {
	return &RepositoryImplementation[T]{db: db.Clauses(onConflict)}
}

func (r *RepositoryImplementation[T]) GetAll(filter filter.ForRepository) ([]T, error) {
	var contents []T

	query := filter.Apply(r.db)

	if filter.Aggregate() {
		query = r.instance.Preload(query)
	}

	err := query.Find(&contents).Error
	return contents, err
}

func (r *RepositoryImplementation[T]) GetByID(id uint) (*T, error) {
	var content *T

	query := r.instance.Preload(r.db)

	err := query.Find(&content, id).Error
	return content, err
}

func (r *RepositoryImplementation[T]) Create(content *T) error {
	return r.db.Create(content).Error
}

func (r *RepositoryImplementation[T]) CreateInBatches(contents []T, batchSize int) error {
	return r.db.CreateInBatches(contents, batchSize).Error
}

func (r *RepositoryImplementation[T]) Update(content *T) error {
	return r.db.Save(content).Error
}

func (r *RepositoryImplementation[T]) UpdateInBatches(contents []T, batchSize int) error {
	return r.db.Save(contents).Error
}

func (r *RepositoryImplementation[T]) Delete(id uint) error {
	return r.db.Delete(id).Error
}

func (r *RepositoryImplementation[T]) DeleteInBatches(ids []uint) error {
	for _, id := range ids {
		if err := r.db.Delete(r.instance, id).Error; err != nil {
			return err
		}
	}
	return nil
}

func (r *RepositoryImplementation[T]) Query() *gorm.DB {
	return r.db
}
